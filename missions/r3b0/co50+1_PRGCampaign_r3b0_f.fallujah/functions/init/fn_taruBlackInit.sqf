private ["_obj","_type"];
_obj = _this select 0;
_type = _this select 1;

switch (_type) do {
    case "taru": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_base_01_black_co.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_base_02_black_co.paa"
		];
	};
	
	case "bench": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_bench_black_CO.paa"
		];
	};
	
	case "fuel": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_fuel_black_CO.paa"
		];
	};
	
	case "transport": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_black_CO.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_black_CO.paa"
		];
	};
	
	case "medical": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_black_CO.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_black_CO.paa"
		];
	};
	
	case "ammo": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_black_CO.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_black_CO.paa"
		];
	};
	
	case "cargo": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_black_CO.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_black_CO.paa"
		];
	};
	
	case "repair": {
		_obj setObjectTextureGlobal [
			0, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_black_CO.paa"
		];
		_obj setObjectTextureGlobal [
			1, "\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_black_CO.paa"
		];
	};
	
    default{};
};