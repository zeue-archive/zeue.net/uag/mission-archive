private ["_box","_arsenalAction"];
_box = _this select 0;

_box allowDamage false;
clearWeaponCargoGlobal _box;
clearMagazineCargoGlobal _box;
clearBackpackCargoGlobal _box;
clearItemCargoGlobal _box;

arsenalBoxAction = _box addaction ["<t color='#ffff00'>BIS Arsenal</t>",{[player,true] call BIS_fnc_arsenal;}];
openPRGarsenal = _box addaction ["<t color='#00ff00'>PRG Arsenal</t>",{createDialog "ars_Dialog";}]; [_box, true] call ace_arsenal_fnc_initBox;