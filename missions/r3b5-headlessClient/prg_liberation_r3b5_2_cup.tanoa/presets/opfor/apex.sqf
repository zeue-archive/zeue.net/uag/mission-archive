/*
Needed Mods:
- None

Optional Mods:
- None
*/

// Enemy infantry classes
opfor_officer = "CUP_B_USMC_Officer";										//Officer
opfor_squad_leader = "CUP_B_USMC_Soldier_SL";								//Squad Leader
opfor_team_leader = "CUP_B_USMC_Soldier_TL";									//Team Leader
opfor_sentry = "CUP_B_USMC_SpecOps";											//Rifleman
opfor_rifleman = "CUP_B_USMC_Soldier";										//Rifleman
opfor_rpg = "CUP_B_USMC_Soldier_AT";										//Rifleman (LAT)
opfor_grenadier = "CUP_B_USMC_Soldier_GL";									//Grenadier
opfor_machinegunner = "CUP_B_USMC_Soldier_AR";								//Autorifleman
opfor_heavygunner = "CUP_B_USMC_Soldier_MG";									//Autorifleman
opfor_marksman = "CUP_B_USMC_Soldier_Marksman";										//Marksman
opfor_sharpshooter = "CUP_B_USMC_Soldier_Marksman";									//Marksman
opfor_sniper = "CUP_B_USMC_Sniper_M40A3";											//Sniper
opfor_at = "CUP_B_USMC_Soldier_HAT";											//AT Specialist
opfor_aa = "CUP_B_USMC_Soldier_AA";											//AA Specialist
opfor_medic = "CUP_B_USMC_Medic";											//Combat Life Saver
opfor_engineer = "CUP_B_USMC_Engineer";										//Engineer
opfor_paratrooper = "CUP_B_USMC_Soldier";									//Paratrooper

// Enemy vehicles used by secondary objectives.
opfor_mrap = ["CUP_B_M1151_M2_USMC", "CUP_B_M1151_Deploy_USMC", "CUP_B_HMMWV_M2_USMC"] call BIS_fnc_selectRandom;									//Qilin
opfor_mrap_armed = ["CUP_B_M1151_M2_USMC", "CUP_B_M1151_Deploy_USMC", "CUP_B_HMMWV_M2_USMC"] call BIS_fnc_selectRandom;								//Qilin (HMG)
opfor_transport_helo = "CUP_B_MH60S_USMC";					//Mi-290 Taru (Bench)
opfor_transport_truck = "B_Truck_01_covered_F";					//Tempest Transport (Covered)
opfor_ammobox_transport = "B_Truck_01_transport_F";				//Tempest Transport (Open) -> Has to be able to transport resource crates!
opfor_fuel_truck = "B_Truck_01_fuel_F";							//Tempest Fuel
opfor_ammo_truck = "B_Truck_01_ammo_F";							//Tempest Ammo
opfor_fuel_container = "B_Slingload_01_Fuel_F";				//Taru Fuel Pod
opfor_ammo_container = "B_Slingload_01_Ammo_F";				//Taru Ammo Pod
opfor_flag = "Flag_US_F";												//CSAT Flag

/* Adding a value to these arrays below will add them to a one out of however many in the array, random pick chance.
Therefore, adding the same value twice or three times means they are more likely to be chosen more often. */

/* Militia infantry. Lightweight soldier classnames the game will pick from randomly as sector defenders.
Think of them like garrison or military police forces, which are more meant to control the local population instead of fighting enemy armies. */
militia_squad = [
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman (Lite)
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman (Lite)
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman
	"CUP_B_BAF_Soldier_AT_WDL",													//Rifleman (AT)
	"CUP_B_BAF_Soldier_AR_WDL",													//Autorifleman
	"CUP_B_BAF_Soldier_Marksman_WDL",													//Marksman
	"CUP_B_BAF_Medic_WDL",												//Medic
	"CUP_B_BAF_Officer_WDL"													//Engineer
];

// Militia vehicles. Lightweight vehicle classnames the game will pick from randomly as sector defenders.
militia_vehicles = [
	"CUP_B_Ridgback_GMG_GB_W",
	"CUP_B_Ridgback_GMG_GB_W",
	"CUP_B_MCV80_GB_W_SLAT",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W"
];

// All enemy vehicles that can spawn as sector defenders and patrols at high enemy combat readiness (aggression levels)
opfor_vehicles = [
"CUP_B_M1151_M2_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_HMMWV_MK19_USMC",
"CUP_B_HMMWV_TOW_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_M1165_GMV_USMC",
"CUP_B_M1167_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_RG31_M2_OD_USMC",
"CUP_B_RG31_Mk19_OD_USMC",
"CUP_B_AAV_USMC",
"CUP_B_AAV_Unarmed_USMC",
"CUP_B_LAV25_HQ_USMC",
"CUP_B_HMMWV_Avenger_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_LAV25_USMC",
"CUP_B_LAV25M240_USMC",
"CUP_B_M60A3_USMC",
"CUP_B_M1A1_Woodland_USMC"
];

// All enemy vehicles that can spawn as sector defenders and patrols but at a lower enemy combat readiness (aggression levels).
opfor_vehicles_low_intensity = [
"CUP_B_M1151_M2_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_HMMWV_MK19_USMC",
"CUP_B_HMMWV_TOW_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_M1165_GMV_USMC",
"CUP_B_M1167_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_RG31_M2_OD_USMC",
"CUP_B_RG31_Mk19_OD_USMC",
"CUP_B_AAV_USMC",
"CUP_B_AAV_Unarmed_USMC",
"CUP_B_LAV25_HQ_USMC",
"CUP_B_HMMWV_Avenger_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at high enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles = [
"CUP_B_M1151_M2_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_HMMWV_MK19_USMC",
"CUP_B_HMMWV_TOW_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_M1165_GMV_USMC",
"CUP_B_M1167_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_RG31_M2_OD_USMC",
"CUP_B_RG31_Mk19_OD_USMC",
"CUP_B_AAV_USMC",
"CUP_B_AAV_Unarmed_USMC",
"CUP_B_LAV25_HQ_USMC",
"CUP_B_HMMWV_Avenger_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_UH1Y_Gunship_Dynamic_USMC",
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_MH60S_USMC",
"CUP_B_AH1Z_Dynamic_USMC",
"CUP_B_F35B_USMC",
"CUP_B_AV8B_DYN_USMC",
"CUP_B_A10_DYN_USA",
"CUP_B_AH1Z_Dynamic_USMC"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at lower enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles_low_intensity = [
"CUP_B_M1151_M2_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_HMMWV_MK19_USMC",
"CUP_B_HMMWV_TOW_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_M1165_GMV_USMC",
"CUP_B_M1167_USMC",
"CUP_B_M1151_Mk19_USMC",
"CUP_B_RG31_M2_OD_USMC",
"CUP_B_RG31_Mk19_OD_USMC",
"CUP_B_AAV_USMC",
"CUP_B_AAV_Unarmed_USMC",
"CUP_B_LAV25_HQ_USMC",
"CUP_B_HMMWV_Avenger_USMC",
"CUP_B_M1151_Deploy_USMC",
"CUP_B_HMMWV_M2_USMC",
"CUP_B_HMMWV_M1114_USMC",
"CUP_B_UH1Y_Gunship_Dynamic_USMC",
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_MH60S_USMC"
];

/* All vehicles that spawn within battlegroups (see the above 2 arrays) and also hold 8 soldiers as passengers.
If something in this array can't hold all 8 soldiers then buggy behaviours may occur.	*/
opfor_troup_transports = [
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_MH60S_USMC",
"CUP_B_AAV_USMC",
"CUP_B_AAV_Unarmed_USMC"
];

// Enemy rotary-wings that will need to spawn in flight.
opfor_choppers = [
"CUP_B_AH1Z_Dynamic_USMC",
"CUP_B_UH1Y_Gunship_Dynamic_USMC",
"CUP_B_UH1Y_UNA_USMC",
"CUP_B_MH60S_USMC"
];

// Enemy fixed-wings that will need to spawn in the air.
opfor_air = [
"CUP_B_F35B_USMC",
"CUP_B_AV8B_DYN_USMC",
"CUP_B_A10_DYN_USA"
];
