/*
Needed Mods:
- None

Optional Mods:
- None
*/

// Enemy infantry classes
opfor_officer = "rhsusf_usmc_marpat_wd_officer";										//Officer
opfor_squad_leader = "rhsusf_usmc_marpat_wd_squadleader";								//Squad Leader
opfor_team_leader = "rhsusf_usmc_marpat_wd_leadleader";									//Team Leader
opfor_sentry = "rhsusf_usmc_marpat_wd_rifleman_light";											//Rifleman
opfor_rifleman = "rhsusf_usmc_marpat_wd_rifleman";										//Rifleman
opfor_rpg = "rhsusf_usmc_marpat_wd_smaw";										//Rifleman (LAT)
opfor_grenadier = "rhsusf_usmc_marpat_wd_grenadier";									//Grenadier
opfor_machinegunner = "rhsusf_usmc_marpat_wd_m249";								//Autorifleman
opfor_heavygunner = "rhsusf_usmc_marpat_wd_machinegunner";									//Autorifleman
opfor_marksman = "rhsusf_usmc_marpat_wd_marksman";										//Marksman
opfor_sharpshooter = "rhsusf_usmc_marpat_wd_marksman";									//Marksman
opfor_sniper = "rhsusf_usmc_marpat_wd_sniper";											//Sniper
opfor_at = "rhsusf_usmc_marpat_wd_javelin";											//AT Specialist
opfor_aa = "rhsusf_usmc_marpat_wd_stinger";											//AA Specialist
opfor_medic = "rhsusf_usmc_marpat_wd_rifleman";											//Combat Life Saver
opfor_engineer = "rhsusf_usmc_marpat_wd_explosives";										//Engineer
opfor_paratrooper = "rhsusf_usmc_marpat_wd_squadleader";									//Paratrooper

// Enemy vehicles used by secondary objectives.
opfor_mrap = "rhsusf_rg33_usmc_wd";									//Qilin
opfor_mrap_armed = "rhsusf_rg33_m2_usmc_wd";								//Qilin (HMG)
opfor_transport_helo = "RHS_UH1Y_FFAR";					//Mi-290 Taru (Bench)
opfor_transport_truck = "rhsusf_M1083A1P2_WD_fmtv_usarmy";					//Tempest Transport (Covered)
opfor_ammobox_transport = "rhsusf_M1084A1P2_B_WD_fmtv_usarmy";				//Tempest Transport (Open) -> Has to be able to transport resource crates!
opfor_fuel_truck = "rhsusf_M978A4_BKIT_usarmy_wd";							//Tempest Fuel
opfor_ammo_truck = "rhsusf_M977A4_AMMO_BKIT_usarmy_wd";							//Tempest Ammo
opfor_fuel_container = "B_Slingload_01_Fuel_F";				//Taru Fuel Pod
opfor_ammo_container = "B_Slingload_01_Ammo_F";				//Taru Ammo Pod
opfor_flag = "Flag_US_F";												//CSAT Flag

/* Adding a value to these arrays below will add them to a one out of however many in the array, random pick chance.
Therefore, adding the same value twice or three times means they are more likely to be chosen more often. */

/* Militia infantry. Lightweight soldier classnames the game will pick from randomly as sector defenders.
Think of them like garrison or military police forces, which are more meant to control the local population instead of fighting enemy armies. */
militia_squad = [
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman (Lite)
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman (Lite)
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman
	"CUP_B_BAF_Soldier_Backpack_WDL",														//Rifleman
	"CUP_B_BAF_Soldier_AT_WDL",													//Rifleman (AT)
	"CUP_B_BAF_Soldier_AR_WDL",													//Autorifleman
	"CUP_B_BAF_Soldier_Marksman_WDL",													//Marksman
	"CUP_B_BAF_Medic_WDL",												//Medic
	"CUP_B_BAF_Officer_WDL"													//Engineer
];

// Militia vehicles. Lightweight vehicle classnames the game will pick from randomly as sector defenders.
militia_vehicles = [
	"CUP_B_Ridgback_GMG_GB_W",
	"CUP_B_Ridgback_GMG_GB_W",
	"CUP_B_MCV80_GB_W_SLAT",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W",
	"CUP_B_LR_MG_GB_W"
];

// All enemy vehicles that can spawn as sector defenders and patrols at high enemy combat readiness (aggression levels)
opfor_vehicles = [
	"RHS_M2A2_BUSKI_WD",
	"rhsusf_m113_usarmy",
	"rhsusf_m113_usarmy",
	"rhsusf_m113_usarmy_MK19",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_Mk19",
	"rhsusf_rg33_m2_usmc_wd",
	"rhsusf_rg33_m2_usmc_wd"
];

// All enemy vehicles that can spawn as sector defenders and patrols but at a lower enemy combat readiness (aggression levels).
opfor_vehicles_low_intensity = [
	"rhsusf_M1117_W",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_Mk19",
	"rhsusf_M1220_M2_usarmy_wd",
	"rhsusf_M1220_M2_usarmy_wd",
	"rhsusf_M1230_MK19_usarmy_wd"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at high enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles = [
	"rhsusf_m1a2sep1tuskiiwd_usarmy",
	"rhsusf_m1a2sep1wd_usarmy",
	"rhsusf_M1232_M2_usarmy_wd",
	"rhsusf_M1232_M2_usarmy_wd",
	"rhsusf_M1237_MK19_usarmy_wd",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"RHS_AH1Z_wd",
	"RHS_UH1Y",
	"RHS_UH1Y",
	"RHS_A10"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at lower enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles_low_intensity = [
	"rhsusf_m1a2sep1wd_usarmy",
	"rhsusf_M1232_M2_usarmy_wd",
	"rhsusf_M1232_M2_usarmy_wd",
	"rhsusf_M1237_MK19_usarmy_wd",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"rhsusf_m1025_w_s_m2",
	"RHS_UH1Y",
	"RHS_UH1Y"
];

/* All vehicles that spawn within battlegroups (see the above 2 arrays) and also hold 8 soldiers as passengers.
If something in this array can't hold all 8 soldiers then buggy behaviours may occur.	*/
opfor_troup_transports = [
	"rhsusf_M1232_M2_usarmy_wd",
	"rhsusf_M1237_MK19_usarmy_wd"
];

// Enemy rotary-wings that will need to spawn in flight.
opfor_choppers = [
	"RHS_UH1Y",										//Mi-290 Taru (Bench)
	"RHS_AH1Z_wd"
];

// Enemy fixed-wings that will need to spawn in the air.
opfor_air = [
	"RHS_A10"
];
