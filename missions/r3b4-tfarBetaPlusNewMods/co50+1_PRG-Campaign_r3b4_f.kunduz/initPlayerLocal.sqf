//fixes broken doors for kunduz
inGameUISetEventHandler ["action","
  if (_this select 4 == 'Close Door') then {
  	_intersects = [cursorTarget, 'VIEW'] intersect [ASLToATL eyepos player, (screentoworld [0.5,0.5])];
  	{_intersects pushBack _x} forEach  ([cursorTarget, 'GEOM'] intersect [ASLToATL eyepos player, (screentoworld [0.5,0.5])]);
      _select_door = format ['%1_rot', (_intersects select 0) select 0];
  	cursorObject animate [_select_door, 0];true
  };
 if (_this select 4 == 'Open Door') then {
 	_intersects = [cursorTarget, 'VIEW'] intersect [ASLToATL eyepos player, (screentoworld [0.5,0.5])];
 	{_intersects pushBack _x} forEach  ([cursorTarget, 'GEOM'] intersect [ASLToATL eyepos player, (screentoworld [0.5,0.5])]);
    _select_door = format ['%1_rot', (_intersects select 0) select 0];
  	cursorObject animate [_select_door, 1];true
  };
"];