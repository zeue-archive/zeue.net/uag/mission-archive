/*
Needed Mods:
- CUP Units

Optional Mods:
- None
*/

/* Classnames of the guerilla faction which is friendly or hostile, depending on the civil reputation 
Standard loadout of the units will be replaced with a scripted one, which depends on the guerilla strength, after spawn */
KP_liberation_guerilla_units = [
	"CUP_I_UN_CDF_Soldier_DST"
];

// Armed vehicles
KP_liberation_guerilla_vehicles = [
	"CUP_I_M113_UN"
];

// transport vehicles (should be able to transport at least 10 soldiers)
KP_liberation_guerilla_transports = [
	"CUP_I_Ural_UN"
];

/* Guerilla Equipment
There are 3 tiers for every category. If the strength of the guerillas will increase, they'll have higher tier equipment. */

/* Weapons - You've to add the weapons as array like
["Weaponclassname","Magazineclassname","magazine amount","optic","tripod"]
You can leave optic and tripod empty with "" */
KP_liberation_guerilla_weapons_1 =[
	["CUP_arifle_AK74_GL_kobra","CUP_30Rnd_545x39_AK_M",8,"cup_optic_kobra",""]
];

KP_liberation_guerilla_weapons_2 =[
	["CUP_arifle_AK74_GL_kobra","CUP_30Rnd_545x39_AK_M",8,"cup_optic_kobra",""]
];

KP_liberation_guerilla_weapons_3 =[
	["CUP_arifle_AK74_GL_kobra","CUP_30Rnd_545x39_AK_M",8,"cup_optic_kobra",""]
];

// Uniforms
KP_liberation_guerilla_uniforms_1 = [
	"CUP_U_I_UNO_DST_1"
];
	
KP_liberation_guerilla_uniforms_2 = [
	"CUP_U_I_UNO_DST_1"
];

KP_liberation_guerilla_uniforms_3 = [
	"CUP_U_I_UNO_DST_1"
];

// Vests
KP_liberation_guerilla_vests_1 = [
	"CUP_V_CDF_6B3_1_DST"
];

KP_liberation_guerilla_vests_2 = [
	"CUP_V_CDF_6B3_1_DST"
];

KP_liberation_guerilla_vests_3 = [
	"CUP_V_CDF_6B3_1_DST"
];

// Headgear
KP_liberation_guerilla_headgear_1 = [
	"CUP_H_CDF_H_PASGT_UN"
];

KP_liberation_guerilla_headgear_2 = [
	"CUP_H_CDF_H_PASGT_UN"
];

KP_liberation_guerilla_headgear_3 = [
	"CUP_H_CDF_H_PASGT_UN"
];

// Facegear. Applies for tier 2 and 3.
KP_liberation_guerilla_facegear = [
	"VSM_Balaclava_tan_Goggles"
];
