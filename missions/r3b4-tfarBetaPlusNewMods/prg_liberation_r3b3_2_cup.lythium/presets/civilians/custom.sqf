/*
Needed Mods:
- CUP Units

Optional Mods:
- None
*/

// Civilian classnames.
civilians = [
	"CUP_C_C_Citizen_02",
	"CUP_C_C_Citizen_03",
	"CUP_C_C_Citizen_04",
	"CUP_C_C_Worker_01",
	"CUP_C_C_Worker_02",
	"CUP_C_C_Worker_03",
	"CUP_C_C_Worker_04",
	"CUP_C_C_Profiteer_01",
	"CUP_C_C_Profiteer_02",
	"CUP_C_C_Profiteer_03",
	"CUP_C_C_Profiteer_04",
	"CUP_C_C_Woodlander_01",
	"CUP_C_C_Woodlander_02",
	"CUP_C_C_Woodlander_03",
	"CUP_C_C_Woodlander_04",
	"CUP_C_C_Villager_01",
	"CUP_C_C_Villager_02",
	"CUP_C_C_Villager_03",
	"CUP_C_C_Villager_04",
	"CUP_C_C_Priest_01",
	"CUP_C_C_Policeman_01",
	"CUP_C_C_Policeman_02",
	"CUP_C_C_Functionary_01",
	"CUP_C_C_Functionary_02",
	"CUP_C_C_Doctor_01",
	"CUP_C_C_Schoolteacher_01",
	"CUP_C_C_Assistant_01",
	"CUP_C_C_Rocker_01",
	"CUP_C_C_Rocker_02",
	"CUP_C_C_Rocker_03",
	"CUP_C_C_Rocker_04",
	"CUP_C_C_Mechanic_01",
	"CUP_C_C_Mechanic_02",
	"CUP_C_C_Mechanic_03",
	"CUP_C_C_Worker_05",
	"CUP_C_C_Fireman_01",
	"CUP_C_C_Rescuer_01"
];

// Civilian vehicle classnames.
civilian_vehicles = [
	"CUP_C_LR_Transport_CTK",
	"CUP_C_Octavia_CIV",
	"CUP_C_Skoda_White_CIV",
	"CUP_C_Skoda_Red_CIV",
	"CUP_C_Skoda_Blue_CIV",
	"CUP_C_Skoda_Green_CIV",
	"CUP_C_UAZ_Unarmed_TK_CIV",
	"CUP_C_UAZ_Open_TK_CIV",
	"CUP_C_Ural_Civ_01",
	"CUP_C_Ural_Civ_02",
	"CUP_C_Ural_Civ_03",
	"CUP_C_Ural_Open_Civ_01",
	"CUP_C_Ural_Open_Civ_02",
	"CUP_C_Ural_Open_Civ_03",
	"CUP_C_V3S_Open_TKC",
	"CUP_C_V3S_Covered_TKC",
	"CUP_C_Datsun",
	"CUP_C_Datsun_4seat",
	"CUP_C_Datsun_Plain",
	"CUP_C_Datsun_Covered",
	"CUP_C_Datsun_Tubeframe",
	"CUP_C_Golf4_red_Civ",
	"CUP_C_Golf4_black_Civ",
	"CUP_C_Golf4_yellow_Civ",
	"CUP_C_Golf4_blue_Civ",
	"CUP_C_Golf4_white_Civ",
	"CUP_C_Golf4_green_Civ",
	"CUP_C_Golf4_random_Civ",
	"CUP_C_Golf4_whiteblood_Civ",
	"CUP_C_Golf4_camo_Civ",
	"CUP_C_Golf4_camodigital_Civ",
	"CUP_C_Golf4_camodark_Civ",
	"CUP_C_Golf4_reptile_Civ",
	"CUP_C_Golf4_kitty_Civ",
	"CUP_C_Golf4_crowe_Civ",
	"CUP_C_SUV_TK",
	"CUP_C_SUV_CIV"
];
