/*
Needed Mods:
- CUP Units

Optional Mods:
- None
*/

// Enemy infantry classes
opfor_officer = "CUP_O_TK_INS_Commander";								//Officer (Warlord)
opfor_squad_leader = "CUP_O_TK_INS_Soldier_TL";							//Squad Leader (Warlord)
opfor_team_leader = "CUP_O_TK_INS_Soldier_TL";							//Team Leader (Chief)
opfor_sentry = "CUP_O_TK_INS_Soldier_Enfield";							//Rifleman (AKM)
opfor_rifleman = "CUP_O_TK_INS_Soldier";					//Rifleman (AK-74)
opfor_rpg = "CUP_O_TK_INS_Soldier_AT";									//Rifleman (RPG-7)
opfor_grenadier = "CUP_O_TK_INS_Soldier_GL";							//Grenadier (AKM GP-25)
opfor_machinegunner = "CUP_O_TK_INS_Soldier_AR";					//Autorifleman (AR_Asst)
opfor_heavygunner = "CUP_O_TK_INS_Soldier_MG";							//Heavy Gunner (PKM)
opfor_marksman = "CUP_O_TK_INS_Sniper";					//Marksman (Lee Enfield)
opfor_sharpshooter = "CUP_O_TK_INS_Sniper";					//Sharpshooter (SVD)
opfor_sniper = "CUP_O_TK_INS_Sniper";						//Sniper (SVD)
opfor_at = "CUP_O_TK_INS_Soldier_AT";									//AT Specialist (RPG-7)
opfor_aa = "CUP_O_TK_INS_Soldier_AA";									//AA Specialist (RPG-7)
opfor_medic = "CUP_O_TK_INS_Guerilla_Medic";							//Combat Life Saver (Bonesetter)
opfor_engineer = "CUP_O_TK_INS_Mechanic";						//Engineer (Bomber)
opfor_paratrooper = "CUP_O_TK_INS_Soldier_FNFAL";					//Paratrooper (AKM PM-63)

// Enemy vehicles used by secondary objectives.
opfor_mrap = "CUP_O_LR_Transport_TKM";									//Offroad
opfor_mrap_armed = "CUP_O_LR_MG_TKM";							//Offroad (M2)
opfor_transport_helo = "CUP_O_UH1H_TKA";							//Mi-8MT (Cargo)
opfor_transport_truck = "CUP_O_V3S_Covered_TKM";									//Ural-4320 (Covered)
opfor_ammobox_transport = "CUP_O_V3S_Open_TKM";							//Ural-4320 (Open) -> Has to be able to transport resource crates!
opfor_fuel_truck = "CUP_O_V3S_Refuel_TKM";								//Ural-4320 (Fuel)
opfor_ammo_truck = "CUP_O_V3S_Rearm_TKM";								//GAZ-66 (Ammo)
opfor_fuel_container = "Land_Pod_Heli_Transport_04_fuel_F";				//Taru Fuel Pod
opfor_ammo_container = "Land_Pod_Heli_Transport_04_ammo_F";				//Taru Ammo Pod
opfor_flag = "";											//Takistan Flag

/* Adding a value to these arrays below will add them to a one out of however many in the array, random pick chance.
Therefore, adding the same value twice or three times means they are more likely to be chosen more often. */

/* Militia infantry. Lightweight soldier classnames the game will pick from randomly as sector defenders.
Think of them like garrison or military police forces, which are more meant to control the local population instead of fighting enemy armies. */
militia_squad = [
	"CUP_O_TK_INS_Soldier",										//Rifleman (AKM)
	"CUP_O_TK_INS_Soldier",										//Rifleman (AKM)
	"CUP_O_TK_INS_Soldier",									//Rifleman (AKM-74)
	"CUP_O_TK_INS_Soldier",									//Rifleman (AKM-74)
	"CUP_O_TK_INS_Soldier_AT",											//Rifleman (RPG-7)
	"CUP_O_TK_INS_Soldier_AR",											//Autorifleman (PKM)
	"CUP_O_TK_INS_Sniper",									//Marksman (Lee Enfield)
	"CUP_O_TK_INS_Guerilla_Medic",										//Medic (Bonesetter)
	"CUP_O_TK_INS_Mechanic"										//Engineer (Bomber)
];

// Militia vehicles. Lightweight vehicle classnames the game will pick from randomly as sector defenders.
militia_vehicles = [
	"CUP_O_LR_MG_TKM"												//UAZ-3151 (DShKM)
];

// All enemy vehicles that can spawn as sector defenders and patrols at high enemy combat readiness (aggression levels).
opfor_vehicles = [
	"CUP_O_LR_MG_TKM",
	"CUP_O_LR_MG_TKM",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_ATGM_TKA",
	"CUP_O_BTR60_TK",
	"CUP_O_BMP1_TKA",
	"CUP_O_BMP2_TKA",
	"CUP_O_T34_TKA"
];

// All enemy vehicles that can spawn as sector defenders and patrols but at a lower enemy combat readiness (aggression levels).
opfor_vehicles_low_intensity = [
	"CUP_O_LR_MG_TKM",
	"CUP_O_LR_MG_TKM",
	"CUP_O_LR_MG_TKM",
	"CUP_O_BRDM2_TKA"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at high enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles = [
	"CUP_O_LR_MG_TKM",
	"CUP_O_LR_MG_TKM",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_ATGM_TKA",
	"CUP_O_BTR60_TK",
	"CUP_O_BMP1_TKA",
	"CUP_O_BMP2_TKA",
	"CUP_O_T34_TKA"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at lower enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles_low_intensity = [
	"CUP_O_LR_MG_TKM",
	"CUP_O_LR_MG_TKM",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_TKA",
	"CUP_O_BRDM2_ATGM_TKA",
	"CUP_O_BTR60_TK",
	"CUP_O_BMP1_TKA",
	"CUP_O_BMP2_TKA",
	"CUP_O_T34_TKA"
];

/* All vehicles that spawn within battlegroups (see the above 2 arrays) and also hold 8 soldiers as passengers.
If something in this array can't hold all 8 soldiers then buggy behaviours may occur.	*/
opfor_troup_transports = [
	"CUP_O_V3S_Covered_TKM"											//Mi-8MT (Cargo)
];

// Enemy rotary-wings that will need to spawn in flight.
opfor_choppers = [
	"CUP_O_UH1H_TKA"
];

// Enemy fixed-wings that will need to spawn in the air.
opfor_air = [
	"CUP_O_L39_TK"
];
